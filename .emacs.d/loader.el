(require 'cl)
(setq tls-checktrust t)

(setq python (or (executable-find "py.exe")
                 (executable-find "python")
                 ))

(let ((trustfile
       (replace-regexp-in-string
        "\\\\" "/"
        (replace-regexp-in-string
         "\n" ""
         (shell-command-to-string (concat python " -m certifi"))))))
  (setq tls-program
        (list
         (format "gnutls-cli%s --x509cafile %s -p %%p %%h"
                 (if (eq window-system 'w32) ".exe" "") trustfile)))
  (setq gnutls-verify-error t)
  (setq gnutls-trustfiles (list trustfile)))

;; Test the settings by using the following code snippet:
;;  (let ((bad-hosts
;;         (loop for bad
;;               in `("https://wrong.host.badssl.com/"
;;                    "https://self-signed.badssl.com/")
;;               if (condition-case e
;;                      (url-retrieve
;;                       bad (lambda (retrieved) t))
;;                    (error nil))
;;               collect bad)))
;;    (if bad-hosts
;;        (error (format "tls misconfigured; retrieved %s ok" bad-hosts))
;;      (url-retrieve "https://badssl.com"
;;                    (lambda (retrieved) t))))

(require 'package)

;; Modern linux installations will fail with TLS erros if 1.3 is not a priority
(setq gnutls-algorithm-priority "NORMAL:-VERS-TLS1.3")
(defvar gnu '("gnu" . "https://elpa.gnu.org/packages/"))
(defvar melpa '("melpa" . "https://melpa.org/packages/"))
(defvar melpa-stable '("melpa-stable" . "https://stable.melpa.org/packages/"))
(defvar org-elpa '("org" . "https://orgmode.org/elpa/"))

;; Add marmalade to package repos
(setq package-archives nil)
(add-to-list 'package-archives melpa-stable t)
(add-to-list 'package-archives melpa t)
(add-to-list 'package-archives gnu t)
(add-to-list 'package-archives org-elpa t)

(when (< emacs-major-version 27)
  (package-initialize))

(unless (and (file-exists-p (concat init-dir "elpa/archives/gnu"))
             (file-exists-p (concat init-dir "elpa/archives/melpa"))
             (file-exists-p (concat init-dir "elpa/archives/melpa-stable")))
  (package-refresh-contents))

(defun packages-install (&rest packages)
  (message "running packages-install")
  (mapc (lambda (package)
          (let ((name (car package))
                (repo (cdr package)))
            (when (not (package-installed-p name))
              (let ((package-archives (list repo)))
                (when (< emacs-major-version 27)
                  (package-initialize))
                (package-install name)))))
        packages)
  (when (< emacs-major-version 27)
    (package-initialize))
  (delete-other-windows))

;; Install extensions if they're missing
(defun init--install-packages ()
  (message "Lets install some packages")
  (packages-install
   ;; Since use-package this is the only entry here
   ;; ALWAYS try to use use-package!
   (cons 'use-package melpa)
   ))

(condition-case nil
    (init--install-packages)
  (error
   (package-refresh-contents)
   (init--install-packages)))

;; Always install packages - obsolete due to straight.el
   (straight-use-package 'use-package)
   (setq straight-use-package-by-default t)
;; The following is VERY IMPORTANT.  Write hooks using their real name
 ;; instead of a shorter version: after-init ==> `after-init-hook'.
 ;;
 ;; This is to empower help commands with their contextual awareness,
 ;; such as `describe-symbol'.
 (setq use-package-hook-name-suffix nil)
 (straight-use-package '(org :type built-in))
 ;; https://github.com/raxod502/straight.el/issues/211
  ;; (straight-use-package '(org-mode
  ;; :branch "release_9.3"))
 ;; (straight-use-package 'org)
 ;;  (require 'use-package-ensure)
 ;;  (setq use-package-always-ensure t)

(use-package diminish)

(fset 'yes-or-no-p 'y-or-n-p)

(use-package ergoemacs-mode
  :config
  (setq ergoemacs-theme nil)
  (setq ergoemacs-keyboard-layout "dv")
  (ergoemacs-mode 1)
)

(use-package bm
  :demand t

  :init
  ;; restore on load (even before you require bm)
  (setq bm-restore-repository-on-load t)


  :config
  ;; Allow cross-buffer 'next'
  (setq bm-cycle-all-buffers t)

  ;; where to store persistant files
  (setq bm-repository-file (concat user-emacs-directory "/bm-repository"))

  ;; save bookmarks
  (setq-default bm-buffer-persistence t)

  ;; Loading the repository from file when on start up.
  (add-hook 'after-init-hook 'bm-repository-load)

  ;; Saving bookmarks
  (add-hook 'kill-buffer-hook #'bm-buffer-save)

  ;; Saving the repository to file when on exit.
  ;; kill-buffer-hook is not called when Emacs is killed, so we
  ;; must save all bookmarks first.
  (add-hook 'kill-emacs-hook #'(lambda nil
                                 (bm-buffer-save-all)
                                 (bm-repository-save)))

  ;; The `after-save-hook' is not necessary to use to achieve persistence,
  ;; but it makes the bookmark data in repository more in sync with the file
  ;; state.
  (add-hook 'after-save-hook #'bm-buffer-save)

  ;; Restoring bookmarks
  (add-hook 'find-file-hooks   #'bm-buffer-restore)
  (add-hook 'after-revert-hook #'bm-buffer-restore)

  ;; The `after-revert-hook' is not necessary to use to achieve persistence,
  ;; but it makes the bookmark data in repository more in sync with the file
  ;; state. This hook might cause trouble when using packages
  ;; that automatically reverts the buffer (like vc after a check-in).
  ;; This can easily be avoided if the package provides a hook that is
  ;; called before the buffer is reverted (like `vc-before-checkin-hook').
  ;; Then new bookmarks can be saved before the buffer is reverted.
  ;; Make sure bookmarks is saved before check-in (and revert-buffer)
  (add-hook 'vc-before-checkin-hook #'bm-buffer-save)


  :bind (("<f2>" . bm-next)
         ("S-<f2>" . bm-previous)
         ("C-<f2>" . bm-toggle))
  )

(use-package helm
  :diminish helm-mode
  :init
  (progn
    (setq helm-candidate-number-limit 100)
    ;; From https://gist.github.com/antifuchs/9238468
    (setq helm-idle-delay 0.0 ; update fast sources immediately (doesn't).
          helm-input-idle-delay 0.01  ; this actually updates things
                                        ; reeeelatively quickly.
          helm-yas-display-key-on-candidate t
          helm-quick-update t
          helm-M-x-requires-pattern nil
          helm-ff-skip-boring-files t
          helm-M-x-fuzzy-match t)
    (helm-mode))
  :bind
  (("M-x" . helm-M-x)
   ("C-x C-f" . helm-find-files)
   ("C-c h" . helm-mini)
   ("C-h a" . helm-apropos)
   ("C-x C-b" . helm-buffers-list)
   ("C-x b" . helm-buffers-list)
   ("M-y" . helm-show-kill-ring))
  )

(use-package smex
    :ensure t)

;;   (use-package counsel
;;     :bind
;;     (("M-x" . counsel-M-x)
;;      ("M-y" . counsel-yank-pop)
;;      :map ivy-minibuffer-map
;;      ("M-y" . ivy-next-line)))

;;   (use-package swiper
;; ;;    :pin melpa-stable
;;     :diminish ivy-mode

;;     :bind*
;;     (("C-s" . swiper)
;;      ("C-c C-r" . ivy-resume)
;;      ("C-x C-f" . counsel-find-file)
;;      ("C-c h f" . counsel-describe-function)
;;      ("C-c h v" . counsel-describe-variable)
;;      ("C-c i u" . counsel-unicode-char)
;;      ("M-i" . counsel-imenu)
;;      ("C-c g" . counsel-git)
;;      ("C-c j" . counsel-git-grep)
;;      ("C-c k" . counsel-ag)
;;      ;;      ("C-c l" . scounsel-locate)
;;      )
;;     :config
;;     (progn
;;       (ivy-mode 1)
;;       (setq ivy-use-virtual-buffers t)
;;       (define-key read-expression-map (kbd "C-r") #'counsel-expression-history)
;;       (ivy-set-actions
;;        'counsel-find-file
;;        '(("d" (lambda (x) (delete-file (expand-file-name x)))
;;           "delete"
;;           )))
;;       (ivy-set-actions
;;        'ivy-switch-buffer
;;        '(("k"
;;           (lambda (x)
;;             (kill-buffer x)
;;             (ivy--reset-state ivy-last))
;;           "kill")
;;          ("j"
;;           ivy--switch-buffer-other-window-action
;;           "other window")))))

;;   (use-package counsel-projectile
;;     :config
;;     (counsel-projectile-mode))

;;   (use-package ivy-hydra )

(use-package pretty-hydra)

(use-package dashboard
  :init
  (add-hook 'after-init-hook 'dashboard-refresh-buffer)
  (setq dashboard-banner-logo-title "Welcome to Emacs Dashboard. Again.")
  (setq dashboard-startup-banner "/home/glicone/.emacs.d/images/yui.png")
  (setq dashboard-center-content t)
  (setq dashboard-items '((recents  . 5)
                        (bookmarks . 5)
                        (projects . 5)
                        (agenda . 5)
                        (registers . 5)))
  :config
  (dashboard-setup-startup-hook))

(global-set-key (kbd "C-w") 'kill-this-buffer)

(setq mouse-wheel-scroll-amount '(1 ((shift) . 1) ((control) . nil)))
(setq mouse-wheel-progressive-speed nil)

(cua-mode t)

(add-to-list 'load-path "~/.emacs.d/vendor/emacs-powerline")
(use-package powerline
  :ensure t)
;; customization
(setq powerline-arrow-shape 'arrow)   ;; the default
(setq powerline-arrow-shape 'curve)   ;; give your mode-line curves
(setq powerline-arrow-shape 'arrow14) ;; best for small font

(use-package which-key
  :diminish which-key-mode
  :config
  (which-key-mode))

(use-package projectile
  :bind (("C-c p f" . projectile-find-file)
         ("C-c p p" . projectile-switch-project)
         ("C-c p t" . projectile-find-test-file))
  :config
  (setq projectile-enable-caching t)
  (add-hook 'prog-mode-hook 'projectile-mode))

(use-package recentf
  :config
  (setq recentf-save-file   (expand-file-name "recentf" init-dir) )
  (setq recentf-max-saved-items 200)
  (setq recentf-exclude '(".gz" ".xz" ".zip" "/elpa/" "/ssh:" "/sudo:"))

;;   (defun prot/recentf-keep-predicate (file)
;;     "Additional conditions for saving in `recentf-list'.
;; Add this function to `recentf-keep'.

;; NEEDS REVIEW."
;;     (cond
;;      ((file-directory-p file) (file-readable-p file))))
;;   (add-to-list 'recentf-keep 'prot/recentf-keep-default-predicate)

;;   (defun prot/recentf (&optional input)
;;     "Select item from `recentf-list' using completion.
;; Use INPUT as an initial, yet editable, filter.

;; The user's $HOME directory is abbreviated as a tilde."
;;     (interactive)
;;     (let* ((files (mapcar 'abbreviate-file-name recentf-list))
;;            (f (icomplete-vertical-do ()
;;                                      (completing-read "Open recentf entry: " files nil t
;;                                                       (when input input)))))
;;       (find-file f)))

;;   (defun prot/recentf-dirs (&optional arg)
;;     "Select directory from `recentf-list' using completion.
;; With \\[universal-argument] present the list in a `dired' buffer.
;; This buffer is meant to be reused by subsequent invocations of
;; this command (otherwise you need to remove the `when' expression.

;; Without \\[universal-argument], the user's $HOME directory is
;; abbreviated as a tilde.  In the Dired buffer paths are absolute."
;;     (interactive "P")
;;     (let* ((list (mapcar 'abbreviate-file-name recentf-list))
;;            (dirs (delete-dups
;;                   (mapcar (lambda (file)
;;                             (if (file-directory-p file)
;;                                 (directory-file-name file)
;;                               (substring (file-name-directory file) 0 -1)))
;;                           list)))
;;            (buf "*Recentf Dired*")
;;            (default-directory "~"))
;;       (when (get-buffer buf)
;;         (kill-buffer buf))
;;       (if arg
;;           (dired (cons (generate-new-buffer-name buf) dirs))
;;         (icomplete-vertical-do ()
;;                                (find-file
;;                                 (completing-read "Recent dirs: " dirs nil t))))))

  ;; :hook (after-init-hook . recentf-mode)
  ;; :bind (("s-r" . prot/recentf)
  ;;        ("C-x C-r" . prot/recentf-dirs))
  )

(use-package request)

;;(add-to-list 'load-path (expand-file-name (concat init-dir "ox-leanpub")))
;;(load-library "ox-leanpub")
;; (add-to-list 'load-path (expand-file-name (concat init-dir "ox-ghost")))
;; (load-library "ox-ghost")
;;; http://www.lakshminp.com/publishing-book-using-org-mode

;;(defun leanpub-export ()
;;  "Export buffer to a Leanpub book."
;;  (interactive)
;;  (if (file-exists-p "./Book.txt")
;;      (delete-file "./Book.txt"))
;;  (if (file-exists-p "./Sample.txt")
;;      (delete-file "./Sample.txt"))
;;  (org-map-entries
;;   (lambda ()
;;     (let* ((level (nth 1 (org-heading-components)))
;;            (tags (org-get-tags))
;;            (title (or (nth 4 (org-heading-components)) ""))
;;            (book-slug (org-entry-get (point) "TITLE"))
;;            (filename
;;             (or (org-entry-get (point) "EXPORT_FILE_NAME") (concat (replace-regexp-in-string " " "-" (downcase title)) ".md"))))
;;       (when (= level 1) ;; export only first level entries
;;         ;; add to Sample book if "sample" tag is found.
;;         (when (or (member "sample" tags)
;;                   ;;(string-prefix-p "frontmatter" filename) (string-prefix-p "mainmatter" filename)
;;                   )
;;           (append-to-file (concat filename "\n\n") nil "./Sample.txt"))
;;         (append-to-file (concat filename "\n\n") nil "./Book.txt")
;;         ;; set filename only if the property is missing
;;         (or (org-entry-get (point) "EXPORT_FILE_NAME")  (org-entry-put (point) "EXPORT_FILE_NAME" filename))
;;         (org-leanpub-export-to-markdown nil 1 nil)))) "-noexport")
;;  (org-save-all-org-buffers)
;;  nil
;;  nil)
;;
;;(require 'request)
;;
;;(defun leanpub-preview ()
;;  "Generate a preview of your book @ Leanpub."
;;  (interactive)
;;  (request
;;   "https://leanpub.com/clojure-on-the-server/preview.json" ;; or better yet, get the book slug from the buffer
;;   :type "POST"                                             ;; and construct the URL
;;   :data '(("api_key" . ""))
;;   :parser 'json-read
;;   :success (function*
;;             (lambda (&key data &allow-other-keys)
;;               (message "Preview generation queued at leanpub.com.")))))

(setq TeX-auto-save t)
(setq TeX-parse-self t)
(setq-default TeX-master nil)

(use-package langtool
  :config (setq langtool-language-tool-server-jar (concat user-emacs-directory "/LanguageTool-4.8/languagetool-server.jar"))
  :bind (("\C-x4w" . langtool-check)
         ("\C-x4W" . langtool-check-done)
         ("\C-x4l" . langtool-switch-default-language)
         ("\C-x44" . langtool-show-message-at-point)
         ("\C-x4c" . langtool-correct-buffer)))

(dolist (hook '(text-mode-hook))
  (add-hook hook (lambda ()
             ;;    (flyspell-mode 1)
                   (visual-line-mode 1)
                   )))

(use-package emacs
  :ensure nil
  :config
  (setq-default scroll-preserve-screen-position t)
  (setq-default scroll-conservatively 1) ; affects `scroll-step'
  (setq-default scroll-margin 0)

  (define-minor-mode prot/scroll-centre-cursor-mode
    "Toggle centred cursor scrolling behaviour."
    :init-value nil
    :lighter " S="
    :global nil
    (if prot/scroll-centre-cursor-mode
        (setq-local scroll-margin (* (frame-height) 2)
                    scroll-conservatively 0
                    maximum-scroll-margin 0.5)
      (dolist (local '(scroll-preserve-screen-position
                       scroll-conservatively
                       maximum-scroll-margin
                       scroll-margin))
        (kill-local-variable `,local))))

  ;; C-c l is used for `org-store-link'.  The mnemonic for this is to
  ;; focus the Line and also works as a variant of C-l.
  :bind ("C-c L" . prot/scroll-centre-cursor-mode))

(use-package flyspell-popup
:config
(define-key flyspell-mode-map (kbd "C-;") #'flyspell-popup-correct))

(use-package markdown-mode
:ensure t
:commands (markdown-mode gfm-mode)
:mode (("README\\.md\\'" . gfm-mode)
       ("\\.md\\'" . markdown-mode)
       ("\\.markdown\\'" . markdown-mode))
:init (setq markdown-command "multimarkdown"))

(use-package htmlize)

(defun my/with-theme (theme fn &rest args)
  (let ((current-themes custom-enabled-themes))
    (mapcar #'disable-theme custom-enabled-themes)
    (load-theme theme t)
    (let ((result (apply fn args)))
      (mapcar #'disable-theme custom-enabled-themes)
      (mapcar (lambda (theme) (load-theme theme t)) current-themes)
      result)))

;; (advice-add #'org-export-to-file :around (apply-partially #'my/with-theme 'doom-one))
;; (advice-add #'org-export-to-buffer :around (apply-partially #'my/with-theme 'doom-one))

(use-package ox-hugo)

(use-package undo-tree
  :init
  (global-undo-tree-mode)
  :config
  (setq undo-tree-visualizer-diff t)
  (setq undo-tree-visualizer-timestamps t))

(defun unfill-paragraph (&optional region)
      "Takes a multi-line paragraph and makes it into a single line of text."
      (interactive (progn (barf-if-buffer-read-only) '(t)))
      (let ((fill-column (point-max))
            ;; This would override `fill-column' if it's an integer.
            (emacs-lisp-docstring-fill-column t))
        (fill-paragraph nil region)))

;; Handy key definition
    (define-key global-map "\M-Q" 'unfill-paragraph)

(use-package expand-region
  :config
  (global-set-key (kbd "C-=") 'er/expand-region))

;; (setq fill-column 100)
;;   (use-package visual-fill-column
;;     :config
;;     (add-hook 'visual-line-mode-hook #'visual-fill-column-mode))

(use-package olivetti
  :diminish
  :config
  (setq olivetti-body-width 0.65)
  (setq olivetti-minimum-body-width 72)
  (setq olivetti-recall-visual-line-mode-entry-state t)
  :bind ("C-c o" . olivetti-mode)
)

(setq-default indent-tabs-mode nil)

(use-package s)

(use-package hydra)

(use-package hideshow
  :bind (("C->" . my-toggle-hideshow-all)
         ("C-<" . hs-hide-level)
         ("C-;" . hs-toggle-hiding))
  :config
  ;; Hide the comments too when you do a 'hs-hide-all'
  (setq hs-hide-comments nil)
  ;; Set whether isearch opens folded comments, code, or both
  ;; where x is code, comments, t (both), or nil (neither)
  (setq hs-isearch-open t)
  ;; Add more here

  (setq hs-set-up-overlay
        (defun my-display-code-line-counts (ov)
          (when (eq 'code (overlay-get ov 'hs))
            (overlay-put ov 'display
                         (propertize
                          (format " ... <%d> "
                                  (count-lines (overlay-start ov)
                                               (overlay-end ov)))
                          'face 'font-lock-type-face)))))

  (defvar my-hs-hide nil "Current state of hideshow for toggling all.")
       ;;;###autoload
  (defun my-toggle-hideshow-all () "Toggle hideshow all."
         (interactive)
         (setq my-hs-hide (not my-hs-hide))
         (if my-hs-hide
             (hs-hide-all)
           (hs-show-all)))

  (add-hook 'prog-mode-hook (lambda ()
                              (hs-minor-mode 1)
                              )))

(global-prettify-symbols-mode 1)

(use-package paredit
  :diminish paredit-mode
  :config
  (add-hook 'emacs-lisp-mode-hook       #'enable-paredit-mode)
  (add-hook 'eval-expression-minibuffer-setup-hook #'enable-paredit-mode)
  (add-hook 'ielm-mode-hook             #'enable-paredit-mode)
  (add-hook 'lisp-mode-hook             #'enable-paredit-mode)
  (add-hook 'lisp-interaction-mode-hook #'enable-paredit-mode)
  (add-hook 'scheme-mode-hook           #'enable-paredit-mode)
  :bind (("C-c d" . paredit-forward-down))
  )

;; Ensure paredit is used EVERYWHERE!
(use-package paredit-everywhere
  :diminish paredit-everywhere-mode
  :config
  (add-hook 'list-mode-hook #'paredit-everywhere-mode))

(use-package highlight-parentheses
  :diminish highlight-parentheses-mode
  :config
  (add-hook 'emacs-lisp-mode-hook
            (lambda()
              (highlight-parentheses-mode)
              )))

(use-package rainbow-delimiters
  :config
  (add-hook 'lisp-mode-hook
            (lambda()
              (rainbow-delimiters-mode)
              )))

(global-highlight-parentheses-mode)

(use-package yasnippet
  :diminish yas
  :hook (go-mode . yas-minor-mode)
  :commands yas-minor-mode
  :config
  (yas/global-mode 1)
  (add-to-list 'yas-snippet-dirs (concat init-dir "snippets")))

(use-package clojure-snippets)
(use-package java-snippets)

(use-package company
  :bind (("C-x /". company-complete))
  :config
  (setq company-idle-delay 10)
  (setq company-minimum-prefix-length 1)
  (global-company-mode)
  )

 (use-package auto-complete
  :ensure t
  :config
  (ac-config-default)
  (global-auto-complete-mode t)
 )

(use-package magit
  :bind (("C-c m" . magit-status)))

(use-package magit-gitflow
  :config
  (add-hook 'magit-mode-hook 'turn-on-magit-gitflow))

(use-package forge)

(use-package git-timemachine)

;; https://github.com/alphapapa/unpackaged.el#smerge-mode
;; Tipped by Mike Z.
(use-package smerge-mode
  :after hydra
  :config
  (defhydra unpackaged/smerge-hydra
    (:color pink :hint nil :post (smerge-auto-leave))
    "
^Move^       ^Keep^               ^Diff^                 ^Other^
^^-----------^^-------------------^^---------------------^^-------
_n_ext       _b_ase               _<_: upper/base        _C_ombine
_p_rev       _u_pper              _=_: upper/lower       _r_esolve
^^           _l_ower              _>_: base/lower        _k_ill current
^^           _a_ll                _R_efine
^^           _RET_: current       _E_diff
"
    ("n" smerge-next)
    ("p" smerge-prev)
    ("b" smerge-keep-base)
    ("u" smerge-keep-upper)
    ("l" smerge-keep-lower)
    ("a" smerge-keep-all)
    ("RET" smerge-keep-current)
    ("\C-m" smerge-keep-current)
    ("<" smerge-diff-base-upper)
    ("=" smerge-diff-upper-lower)
    (">" smerge-diff-base-lower)
    ("R" smerge-refine)
    ("E" smerge-ediff)
    ("C" smerge-combine-with-next)
    ("r" smerge-resolve)
    ("k" smerge-kill-current)
    ("ZZ" (lambda ()
            (interactive)
            (save-buffer)
            (bury-buffer))
     "Save and bury buffer" :color blue)
    ("q" nil "cancel" :color blue))
  :hook (magit-diff-visit-file . (lambda ()
                                   (when smerge-mode
                                     (unpackaged/smerge-hydra/body)))))

(use-package git-gutter+
 :ensure t
 :config
 (global-git-gutter+-mode t))

(use-package git-gutter-fringe+
 :ensure t)

(use-package restclient)

(use-package ob-restclient
  :config
  (org-babel-do-load-languages
   'org-babel-load-languages
   '((restclient . t)
     (shell . t)
     (ditaa . t))))

(use-package hl-todo
  :hook (prog-mode . hl-todo-mode)
  :config
  (setq hl-todo-highlight-punctuation ":"
        hl-todo-keyword-faces
        `(("TODO"       warning bold)
          ("FIXME"      error bold)
          ("HACK"       font-lock-constant-face bold)
          ("REVIEW"     font-lock-keyword-face bold)
          ("NOTE"       success bold)
          ("DEPRECATED" font-lock-doc-face bold))))

(use-package erlang
    :init
    (add-to-list 'auto-mode-alist '("\\.P\\'" . erlang-mode))
    (add-to-list 'auto-mode-alist '("\\.E\\'" . erlang-mode))
    (add-to-list 'auto-mode-alist '("\\.S\\'" . erlang-mode))
    (add-to-list 'auto-mode-alist '("\\.erl?$" . erlang-mode))
    (add-to-list 'auto-mode-alist '("\\.hrl?$" . erlang-mode))
    :config
    (setq erlang-root-dir "/usr/lib64/erlang")
    (add-to-list 'exec-path "/usr/lib64/erlang/bin")
    (setq erlang-man-root-dir "/usr/lib64/erlang/man")

    (add-hook 'erlang-mode-hook
              (lambda ()
                (setq mode-name "erl"
                      erlang-compile-extra-opts '((i . "../include"))
                      erlang-root-dir "/usr/local/lib/erlang"))))


  ;; (use-package edts
;;     :init
;;     (setq edts-inhibit-package-check t
;;           edts-man-root "~/.emacs.d/edts/doc/18.2.1"))

;; (use-package flycheck
;;   :diminish flycheck-mode
;;   :config
;;   (add-hook 'after-init-hook 'global-flycheck-mode)
;;   (setq flycheck-display-errors-function nil
;;         flycheck-erlang-include-path '("../include")
;;         flycheck-erlang-library-path '()
;;         flycheck-check-syntax-automatically '(save)))

  ;;  (defun my-erlang-mode-hook ()
  ;;          ;; when starting an Erlang shell in Emacs, default in the node name
  ;;          (setq inferior-erlang-machine-options '("-sname" "emacs"))
  ;;          ;; add Erlang functions to an imenu menu
  ;;          (imenu-add-to-menubar "imenu")
  ;;          ;; customize keys
  ;;          (local-set-key [return] 'newline-and-indent)
  ;;          ;; intent without tab
  ;;          (function (lambda ()
  ;;                  (setq indent-tabs-mode nil)))
  ;;
  ;;       )

(use-package cider
;;    :pin melpa-stable

    :config
    (add-hook 'cider-repl-mode-hook #'company-mode)
    (add-hook 'cider-mode-hook #'company-mode)
    (add-hook 'cider-mode-hook #'eldoc-mode)
;;    (add-hook 'cider-mode-hook #'cider-hydra-mode)
    (add-hook 'clojure-mode-hook #'paredit-mode)
    (setq cider-repl-use-pretty-printing t)
    (setq cider-repl-display-help-banner nil)
    ;;    (setq cider-cljs-lein-repl "(do (use 'figwheel-sidecar.repl-api) (start-figwheel!) (cljs-repl))")

    :bind (("M-r" . cider-namespace-refresh)
           ;;("C-c r" . cider-repl-reset)
           ("C-c ." . cider-reset-test-run-tests))
    )

  (use-package clj-refactor
    :config
    (add-hook 'clojure-mode-hook (lambda ()
                                   (clj-refactor-mode 1)
                                   ;; insert keybinding setup here
                                   ))
    (cljr-add-keybindings-with-prefix "C-c C-m")
    (setq cljr-warn-on-eval nil)
    :bind ("C-c '" . hydra-cljr-help-menu/body)
    )

;;  (load-library (concat init-dir "cider-hydra.el"))
;;  (require 'cider-hydra)

(use-package web-mode
  :config
  (add-to-list 'auto-mode-alist '("\\.phtml\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.tpl\\.php\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.jsp\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.as[cp]x\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.erb\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.mustache\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.djhtml\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.html?\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.xhtml?\\'" . web-mode))

  (defun my-web-mode-hook ()
    "Hooks for Web mode."
    (setq web-mode-enable-auto-closing t)
    (setq web-mode-enable-auto-quoting t)
    (setq web-mode-markup-indent-offset 2))

  (add-hook 'web-mode-hook  'my-web-mode-hook))

(use-package less-css-mode)

(use-package emmet-mode
  :config
  (add-hook 'clojure-mode-hook 'emmet-mode)
  (add-hook 'html-mode-hook 'emmet-mode)
  (add-hook 'web-mode-hook 'emmet-mode))

(use-package racer
  :config
  (add-hook 'racer-mode-hook #'company-mode)
  (setq company-tooltip-align-annotations t)
  (setq racer-rust-src-path "/home/arjen/.rustup/toolchains/stable-x86_64-unknown-linux-gnu/lib/rustlib/src/rust/src"))

(use-package rust-mode
  :config
  (add-hook 'rust-mode-hook #'racer-mode)
  (add-hook 'racer-mode-hook #'eldoc-mode)
  (setq rust-format-on-save t))

(use-package cargo
  :config
  (setq compilation-scroll-output t)
  (add-hook 'rust-mode-hook 'cargo-minor-mode))

(use-package flycheck-rust
  :config
  (add-hook 'flycheck-mode-hook #'flycheck-rust-setup)
  (add-hook 'rust-mode-hook 'flycheck-mode))

(setq-default tab-width 4)

(use-package go-mode
  :bind (("C-c t t" . go-test-current-test)
         ("C-c t p" . go-test-current-project)
         ("C-c t c" . go-test-current-coverage)
         ("C-c t f" . go-test-current-file))
  :config
  (setq gofmt-command "goimports")
  (add-hook 'before-save-hook 'gofmt-before-save))

(use-package go-guru)

(use-package go-errcheck)

;; Yasnippets
(use-package go-snippets)

;; eldoc integration
(use-package go-eldoc)

;; (use-package gocode
;;   )

;; (use-package godef
;;   )

(use-package gotest)

(use-package flycheck-golangci-lint
  :hook (go-mode . flycheck-golangci-lint-setup))

(use-package typescript-mode)

(setq lsp-clients-angular-language-server-command
  '("node"
    "/home/arjen/.nvm/versions/node/v13.7.0/lib/node_modules/@angular/language-server"
    "--ngProbeLocations"
    "/home/arjen/.nvm/versions/node/v13.7.0/lib/node_modules"
    "--tsProbeLocations"
    "/home/arjen/.nvm/versions/node/v13.7.0/lib/node_modules"
    "--stdio"))

;;(use-package treemacs )
  (use-package lsp-mode
    ;; set prefix for lsp-command-keymap (few alternatives - "C-l", "C-c l")
    :init (setq lsp-keymap-prefix "C-c l")
    :hook (;; replace XXX-mode with concrete major-mode(e. g. python-mode)
           (java-mode . lsp)
           (javascript-mode . lsp-deferred)
           (typescript-mode . lsp-deferred)
           (go-mode . lsp-deferred)
           (rust-mode . lsp-deferred)
           ;; if you want which-key integration
           (lsp-mode . lsp-enable-which-key-integration))
    :commands (lsp lsp-deferred))

  ;; Set up before-save hooks to format buffer and add/delete imports.
  ;; Make sure you don't have other gofmt/goimports hooks enabled.
  (defun lsp-go-install-save-hooks ()
    (add-hook 'before-save-hook #'lsp-format-buffer t t)
    (add-hook 'before-save-hook #'lsp-organize-imports t t))
  (add-hook 'go-mode-hook #'lsp-go-install-save-hooks)

  (use-package lsp-java  :after lsp
    :bind (("C-c l r i". lsp-java-add-import))
    :config
    (add-hook 'java-mode-hook 'lsp)
    (setq lsp-java-server-install-dir (concat user-emacs-directory "/eclipse.jdt.ls/server/"))

    (setq path-to-lombok "/home/arjen/.m2/repository/org/projectlombok/lombok/1.18.10/lombok-1.18.10.jar")

    (setq lsp-java-vmargs
          `("-noverify"
            "-Xmx1G"
            "-XX:+UseG1GC"
            "-XX:+UseStringDeduplication"
            ,(concat "-javaagent:" path-to-lombok)
            ;;,(concat "-Xbootclasspath/a:" path-to-lombok)
            ))

    )

  ;; optionally
  (use-package lsp-ui
    :commands lsp-ui-mode
    :config
    (setq lsp-ui-mode nil)
    (setq lsp-ui-sideline-enable nil)
    (setq lsp-ui-doc-enable nil))


  (use-package company-lsp :commands company-lsp)
  ;; if you are ivy user
  (use-package lsp-ivy :commands lsp-ivy-workspace-symbol)
  (use-package lsp-treemacs :commands lsp-treemacs-errors-list)

;;   ;; optionally if you want to use debugger
;;   (use-package dap-mode
;;     :after lsp-mode
;;     :config
;;     (dap-mode t)
;;     (setq dap-java-test-runner (concat user-emacs-directory "/junit-platform-console-standalone-1.6.0-RC1.jar"))
;;     (add-hook 'dap-stopped-hook
;;               (lambda (arg) (call-interactively #'dap-hydra)))
;;     )
;;   ;; (use-package dap-LANGUAGE) to load the dap adapter for your language
;;
;;   (use-package dap-java
;; ;;    :ensure nil
;;     :after (lsp-java))
;;   ;;(use-package lsp-java-treemacs :after (treemacs))

(defvar java-project-package-roots (list "java/" "test/" "main/" "src/" 1)
    "A list of relative directories (strings) or depths (integer) used by
  `+java-current-package' to delimit the namespace from the current buffer's full
  file path. Each root is tried in sequence until one is found.

  If a directory is encountered in the file path, everything before it (including
  it) will be ignored when converting the path into a namespace.

  An integer depth is how many directories to pop off the start of the relative
  file path (relative to the project root). e.g.

  Say the absolute path is ~/some/project/src/java/net/lissner/game/MyClass.java
  The project root is ~/some/project
  If the depth is 1, the first directory in src/java/net/lissner/game/MyClass.java
    is removed: java.net.lissner.game.
  If the depth is 2, the first two directories are removed: net.lissner.game.")

  (defun java-current-class ()
    "Get the class name for the current file."
    (interactive)
    (unless (eq major-mode 'java-mode)
      (user-error "Not in a java-mode buffer"))
    (unless buffer-file-name
      (user-error "This buffer has no filepath; cannot guess its class name"))
    (or (file-name-sans-extension (file-name-base (buffer-file-name)))
        "ClassName"))

(defun doom-project-root (&optional dir)
  "Return the project root of DIR (defaults to `default-directory').
Returns nil if not in a project."
  (let ((projectile-project-root (unless dir projectile-project-root))
        projectile-require-project-root)
    (projectile-project-root dir)))

  (defun java-current-package ()
    "Converts the current file's path into a namespace.

  For example: ~/some/project/src/net/lissner/game/MyClass.java
  Is converted to: net.lissner.game

  It does this by ignoring everything before the nearest package root (see
  `+java-project-package-roots' to control what this function considers a package
  root)."
    (unless (eq major-mode 'java-mode)
      (user-error "Not in a java-mode buffer"))
    (let* ((project-root (file-truename (doom-project-root)))
           (file-path (file-name-sans-extension
                       (file-truename (or buffer-file-name
                                          default-directory))))
           (src-root (cl-loop for root in java-project-package-roots
                              if (and (stringp root)
                                      (locate-dominating-file file-path root))
                              return (file-name-directory (file-relative-name file-path (expand-file-name root it)))
                              if (and (integerp root)
                                      (> root 0)
                                      (let* ((parts (split-string (file-relative-name file-path project-root) "/"))
                                             (fixed-parts (reverse (nbutlast (reverse parts) root))))
                                        (when fixed-parts
                                          (string-join fixed-parts "/"))))
                              return it)))
      (when src-root
        (string-remove-suffix "." (replace-regexp-in-string "/" "." src-root)))))

;;  (custom-set-variables '(epg-gpg-program  "/usr/local/MacGPG2/bin/gpg2"))

(if (or
     (eq system-type 'darwin)
     (eq system-type 'berkeley-unix))
    (setq system-name (car (split-string system-name "\\."))))

(use-package exec-path-from-shell
  :config
  (when (memq window-system '(mac ns x))
  (exec-path-from-shell-initialize)))

(menu-bar-mode -1)
(tool-bar-mode -1)
(scroll-bar-mode -1)

(setq mac-option-modifier 'none)
(setq mac-command-modifier 'meta)
(setq ns-function-modifier 'hyper)

;; Backup settings
(defvar --backup-directory (concat init-dir "backups"))

(if (not (file-exists-p --backup-directory))
    (make-directory --backup-directory t))

(setq backup-directory-alist `(("." . ,--backup-directory)))
(setq make-backup-files t               ; backup of a file the first time it is saved.
      backup-by-copying t               ; don't clobber symlinks
      version-control t                 ; version numbers for backup files
      delete-old-versions t             ; delete excess backup files silently
      delete-by-moving-to-trash t
      kept-old-versions 6               ; oldest versions to keep when a new numbered backup is made (default: 2)
      kept-new-versions 9               ; newest versions to keep when a new numbered backup is made (default: 2)
      auto-save-default t               ; auto-save every buffer that visits a file
      auto-save-timeout 20              ; number of seconds idle time before auto-save (default: 30)
      auto-save-interval 200            ; number of keystrokes between auto-saves (default: 300)
      )
  (setq delete-by-moving-to-trash t
        trash-directory "~/.Trash/emacs")

  (setq backup-directory-alist `(("." . ,(expand-file-name
                                          (concat init-dir "backups")))))

(setq ns-pop-up-frames nil)

(defun spell-buffer-english ()
  (interactive)
  (ispell-change-dictionary "en_US")
  (flyspell-buffer))

(use-package ispell
  :config
  (when (executable-find "hunspell")
    (setq-default ispell-program-name "hunspell")
    (setq ispell-really-hunspell t))

  ;; (setq ispell-program-name "aspell"
  ;;       ispell-extra-args '("--sug-mode=ultra"))
  :bind (("C-c N" . spell-buffer-dutch)
         ("C-c e" . spell-buffer-english)))

;;; what-face to determine the face at the current point
(defun what-face (pos)
  (interactive "d")
  (let ((face (or (get-char-property (point) 'read-face-name)
                  (get-char-property (point) 'face))))
    (if face (message "Face: %s" face) (message "No face at %d" pos))))

(setq inhibit-startup-message t)
(global-linum-mode 1)
(setq linum-format "%d ")
;;(global-hl-line-mode nil)

(custom-set-faces
 '(line-number-current-line ((t (:inherit default :background "#282635")))))

(setq-default indent-tabs-mode nil)

(defun iwb ()
  "indent whole buffer"
  (interactive)
  (delete-trailing-whitespace)
  (indent-region (point-min) (point-max) nil)
  (untabify (point-min) (point-max)))

(global-set-key (kbd "C-c n") 'iwb)

(electric-pair-mode t)

(use-package all-the-icons)

(use-package all-the-icons-ivy
  :config
  (all-the-icons-ivy-setup))

;; (use-package theme-changer)

;; (setq calendar-location-name "Almere, Netherlands")
;; (setq calendar-latitude 52.22)
;; (setq calendar-longitude 5.9)

;; (change-theme 'modus-operandi 'modus-vivendi)

 (use-package doom-themes

   :config
;;   ;; Global settings (defaults)
   (setq doom-themes-enable-bold t    ; if nil, bold is universally disabled
         doom-themes-enable-italic t) ; if nil, italics is universally disabled

;;   ;; load theme here
    (load-theme 'doom-city-lights t)
;;   ;; Enable flashing mode-line on errors
    (doom-themes-visual-bell-config)

;;   ;; Enable custom neotree theme (all-the-icons must be installed!)
;;   ;;(doom-themes-neotree-config)
;;   ;; or for treemacs users
;;   (setq doom-themes-treemacs-theme "doom-colors") ; use the colorful treemacs theme
;;   (doom-themes-treemacs-config)

   ;; Corrects (and improves) org-mode's native fontification.
   (doom-themes-org-config))

(eval-after-load "org-indent" '(diminish 'org-indent-mode))

;; http://stackoverflow.com/questions/11679700/emacs-disable-beep-when-trying-to-move-beyond-the-end-of-the-document
(defun my-bell-function ())

(setq ring-bell-function 'my-bell-function)
(setq visible-bell nil)

;;  (use-package org)

  (setq org-catch-invisible-edits 'show-and-error)
  (add-hook 'org-mode-hook 'org-indent-mode)

(require 'org-habit)

(add-to-list 'org-modules 'org-habit)

(use-package org-bullets
  :config
  (add-hook 'org-mode-hook (lambda () (org-bullets-mode 1))))

(setq org-hide-emphasis-markers t)

(setq org-bullets-bullet-list '("✙" "♱" "♰" "☥" "✞" "✟" "✝" "†" "✠" "✚" "✜" "✛" "✢" "✣" "✤" "✥"))
(setq org-ellipsis " ➟ ")

(setq org-todo-keywords '((sequence "TODO(t)" "WAITING(w)" "|" "DONE(d)" "CANCELLED(c)")))


(font-lock-add-keywords 'org-mode
                        '(("^ +\\([-*]\\) "
                           (0 (prog1 () (compose-region (match-beginning 1) (match-end 1) "•"))))))

(setq org-link-frame-setup
      (quote
       ((vm . vm-visit-folder-other-frame)
        (vm-imap . vm-visit-imap-folder-other-frame)
        (gnus . org-gnus-no-new-news)
        (file . find-file)
        (wl . wl-other-frame))))

;; From http://www.howardism.org/Technical/Emacs/orgmode-wordprocessor.html
(when (window-system)
;; (let* ((variable-tuple (cond ((x-list-fonts "Source Sans Pro") '(:font "Source Sans Pro"))
;;                                ((x-list-fonts "Lucida Grande")   '(:font "Lucida Grande"))
;;                                ((x-list-fonts "Verdana")         '(:font "Verdana"))
;;                                ((x-family-fonts "Sans Serif")    '(:family "Sans Serif"))
;;                                (nil (warn "Cannot find a Sans Serif Font.  Install Source Sans Pro."))))
;;          (base-font-color     (face-foreground 'default nil 'default))
;;          (headline           `(:inherit default :weight bold :foreground ,base-font-color)))

;;     (custom-theme-set-faces 'user
;;                             `(org-level-8 ((t (,@headline ,@variable-tuple))))
;;                             `(org-level-7 ((t (,@headline ,@variable-tuple))))
;;                             `(org-level-6 ((t (,@headline ,@variable-tuple))))
;;                             `(org-level-5 ((t (,@headline ,@variable-tuple))))
;;                             `(org-level-4 ((t (,@headline ,@variable-tuple :height 1.1))))
;;                             `(org-level-3 ((t (,@headline ,@variable-tuple :height 1.25))))
;;                             `(org-level-2 ((t (,@headline ,@variable-tuple :height 1.5))))
;;                             `(org-level-1 ((t (,@headline ,@variable-tuple :height 1.75))))
;;                             `(org-document-title ((t (,@headline ,@variable-tuple :height 1.5 :underline nil))))))
  )

(use-package dumb-jump
  :ensure t
  :bind (("M-g o" . dumb-jump-go-other-window)
  ("M-g j" . dumb-jump-go)
  ("M-g b" . dumb-jump-back)
  ("M-g q" . dumb-jump-quick-look)
  ("M-g x" . dumb-jump-go-prefer-external)
  ("M-g z" . dumb-jump-go-prefer-external-other-window))
  :config
  (setq dumb-jump-selector 'ivy))

  (use-package ido
  :config
  (ido-mode t)
  (setq ido-enable-flex-matching t))
  ;built-in
  (use-package bs
  :config
  (setq bs-configurations
  '(("files" "^\\*scratch\\*" nil nil bs-visits-non-file bs-sort-buffer-interns-are-last)))
  (global-set-key (kbd "<f2>") 'bs-show))

;; Enable transient mark mode
(transient-mark-mode 1)

(use-package protobuf-mode
  :ensure t
  :mode ("\\.proto\\'" . protobuf-mode))
(put 'upcase-region 'disabled nil)

(add-hook 'before-save-hook 'delete-trailing-whitespace)
